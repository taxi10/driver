package com.test.driver.service;

import com.test.driver.exception.DepartmentException;
import com.test.driver.exception.DriverException;
import com.test.driver.repository.DepartmentRepository;
import com.test.driver.repository.DriverRepository;
import db.entity.Department;
import db.entity.Driver;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class DriverServiceTest {

    @Mock
    private DriverRepository driverRepository;

    @Mock
    private DepartmentRepository departmentRepository;

    @InjectMocks
    private DriverService driverService;

    @Test
    void getDriverByEmail_emailNotExistInDataBase() {
        String email = "Test@mail.ru";
        when(driverRepository.findByEmail(Mockito.anyString()))
                .thenReturn(Optional.empty());


        DriverException driverException = assertThrows(DriverException.class,
                () -> driverService.getDriverByEmail(email));


        assertEquals(DriverException.class, driverException.getClass());
    }

    @Test
    void getDriverByEmail_ok() {
        Driver driver = new Driver();
        driver.setEmail("Test@mail.ru");

        when(driverRepository.findByEmail(Mockito.anyString()))
                .thenReturn(Optional.of(driver));


        Driver driverByEmailActual = driverService.getDriverByEmail(driver.getEmail());


        assertEquals(driver.getEmail(), driverByEmailActual.getEmail());
    }

    @Test
    void saveDriver_invalidEmail() {
        String nameDepartment = "Test";
        Driver driver = new Driver();
        driver.setEmail("Test@yandex.ru");


        DriverException driverException = assertThrows(DriverException.class,
                () -> driverService.saveDriver(driver, nameDepartment));


        assertEquals(DriverException.class, driverException.getClass());
    }

    @Test
    void saveDriver_emailExistsInDataBase() {
        String nameDepartment = "Test";
        Driver driver = new Driver();
        driver.setEmail("Test@mail.ru");

        when(driverRepository.findByEmail(Mockito.anyString()))
                .thenReturn(Optional.of(driver));


        DriverException driverException = assertThrows(DriverException.class,
                () -> driverService.saveDriver(driver, nameDepartment));


        assertEquals(DriverException.class, driverException.getClass());
    }

    @Test
    void saveDriver_departmentNameNotExistInDataBase() {
        String nameDepartment = "Test";
        Driver driver = new Driver();
        driver.setEmail("Test@mail.ru");

        when(driverRepository.findByEmail(Mockito.anyString()))
                .thenReturn(Optional.empty());
        when(departmentRepository.findByName(Mockito.anyString()))
                .thenReturn(Optional.empty());


        DepartmentException departmentException = assertThrows(DepartmentException.class,
                () -> driverService.saveDriver(driver, nameDepartment));


        assertEquals(DepartmentException.class, departmentException.getClass());
    }

    @Test
    void saveDriver_ok() {
        Department department = new Department();
        department.setName("Test");
        Driver driver = new Driver();
        driver.setEmail("Test@mail.ru");
        driver.setDepartment(department);
        driver.setPassword("12345678");

        when(driverRepository.findByEmail(Mockito.anyString()))
                .thenReturn(Optional.empty());
        when(departmentRepository.findByName(Mockito.anyString()))
                .thenReturn(Optional.of(department));
        when(driverRepository.save(Mockito.any(Driver.class)))
                .thenReturn(driver);


        Driver driverActual = driverService.saveDriver(driver, department.getName());


        assertEquals(driver.getEmail(), driverActual.getEmail());
    }

    @Test
    void getDriverFullByEmail_emailNotExistsInDataBase() {
        Driver driver = new Driver();
        driver.setEmail("Test@mail.ru");

        when(driverRepository.findByEmailFullDriver(driver.getEmail()))
                .thenReturn(Optional.empty());


        DriverException driverException = assertThrows(DriverException.class,
                () -> driverService.getDriverFullByEmail(driver.getEmail()));


        assertEquals(DriverException.class, driverException.getClass());
    }

    @Test
    void getDriverFullByEmail_ok() {
        Driver driver = new Driver();
        driver.setEmail("Test@mail.ru");

        when(driverRepository.findByEmailFullDriver(driver.getEmail()))
                .thenReturn(Optional.of(driver));


        Driver driverFullByEmailActual = driverService.getDriverFullByEmail(driver.getEmail());


        assertEquals(driver.getEmail(), driverFullByEmailActual.getEmail());
    }

    @Test
    void updateDriver_emailNotExistsInDataBase() {
        Driver driver = new Driver();
        driver.setEmail("Test@mail.ru");

        when(driverRepository.findByEmailFullDriver(driver.getEmail()))
                .thenReturn(Optional.empty());


        DriverException driverException = assertThrows(DriverException.class,
                () -> driverService.getDriverFullByEmail(driver.getEmail()));


        assertEquals(DriverException.class, driverException.getClass());
    }

    @Test
    void updateDriver_ok() {
        Driver driver = new Driver();
        driver.setEmail("Test@mail.ru");
        driver.setPassword("12345678");

        when(driverRepository.findByEmail(driver.getEmail()))
                .thenReturn(Optional.of(driver));
        when(driverRepository.save(Mockito.any(Driver.class)))
                .thenReturn(driver);


        Driver driverActual = driverService.updateDriver(driver);


        assertEquals(driver.getEmail(), driverActual.getEmail());
    }

    @Test
    void deleteDriverByEmail_emailNotInDataBase() {
        String email = "Test@mail.ru";

        when(driverRepository.findByEmail(Mockito.anyString()))
                .thenReturn(Optional.empty());


        DriverException driverException = assertThrows(DriverException.class,
                () -> driverService.deleteDriverByEmail(email));


        assertEquals(DriverException.class, driverException.getClass());
    }

    @Test
    void deleteDriverByEmail_ok() {
        Driver driver = new Driver();
        driver.setEmail("Test@mail.ru");
        driver.setPassword("12345678");

        when(driverRepository.findByEmail(Mockito.anyString()))
                .thenReturn(Optional.of(driver));


        boolean isDelete = driverService.deleteDriverByEmail(driver.getEmail());


        assertTrue(isDelete);
    }
}