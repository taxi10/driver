package com.test.driver.exception;

public class DriverException extends RuntimeException {
    public DriverException(String message) {
        super(message);
    }
}
