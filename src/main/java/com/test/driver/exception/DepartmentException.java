package com.test.driver.exception;

public class DepartmentException extends RuntimeException {
    public DepartmentException(String message) {
        super(message);
    }
}
