package com.test.driver.exceptionHandler;

import com.test.driver.exception.DepartmentException;
import com.test.driver.exception.DriverException;
import com.test.driver.model.exModel.ExModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@RestControllerAdvice
public class HandlerExceptionController {

    @ExceptionHandler(value = DriverException.class)
    public ResponseEntity<?> userException(DriverException ex) {
        log.debug(ex.getMessage());

        ExModel exceptionModel = ExModel
                .builder()
                .nameError(ex.getClass().getSimpleName())
                .messages(ex.getMessage()).httpStatus(HttpStatus.BAD_REQUEST).build();

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exceptionModel);
    }

    @ExceptionHandler(value = DepartmentException.class)
    public ResponseEntity<?> departmentException(DepartmentException ex) {
        log.debug(ex.getMessage());

        ExModel exceptionModel = ExModel
                .builder()
                .nameError(ex.getClass().getSimpleName())
                .messages(ex.getMessage()).httpStatus(HttpStatus.BAD_REQUEST).build();

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exceptionModel);
    }

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public ResponseEntity<?> valid(MethodArgumentNotValidException ex) {

        String default_message = Stream.of(ex.getMessage().split(";"))
                .map(String::trim)
                .filter(stackTrace -> stackTrace.startsWith("default message"))
                .reduce((first, second) -> second)
                .stream()
                .map(stackTrace -> stackTrace.substring(17, stackTrace.lastIndexOf("]") - 1))
                .collect(Collectors.joining());

        log.debug(default_message);

        ExModel exceptionModel = ExModel
                .builder()
                .nameError(ex.getClass().getSimpleName())
                .messages(default_message).httpStatus(HttpStatus.BAD_REQUEST).build();

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exceptionModel);
    }

}