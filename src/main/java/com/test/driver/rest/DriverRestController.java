package com.test.driver.rest;

import com.test.driver.Config.RabbitMQ;
import com.test.driver.model.restDTO.DriverResponseDTO;
import com.test.driver.service.DriverService;
import com.test.driver.util.mapping.driver.DriverToDriverDTO;
import db.entity.Driver;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Slf4j
@RestController
@RequiredArgsConstructor
public class DriverRestController {

    private final DriverService driverService;

    private final RabbitMQ rabbitMQ;

    @PostMapping(value = "/registration")
    public ResponseEntity<?> driverRegistration(@Valid @RequestBody Driver driver, @RequestParam(value = "department") String department) {
        driverService.saveDriver(driver, department);
        rabbitMQ.emailNotificationsRegistration(driver);
        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "/authentication")
    public ResponseEntity<?> showDriverByEmail(@RequestParam(value = "email") String email) {
        Driver driver = driverService.getDriverByEmail(email.trim());
        DriverResponseDTO build = DriverResponseDTO.builder()
                .email(driver.getEmail())
                .password(driver.getPassword())
                .role(driver.getRole().getRole()).build();
        return ResponseEntity.ok().body(build);
    }


    @GetMapping(value = "/full")
    public ResponseEntity<?> showDriverFullByEmail(@RequestParam(value = "email") String email) {
        Driver driver = driverService.getDriverFullByEmail(email.trim());
        DriverResponseDTO driverResponseDTO = DriverToDriverDTO.convertingDriverToDriverDTO(driver);
        return ResponseEntity.ok().body(driverResponseDTO);
    }

    @PutMapping(value = "/update")
    public ResponseEntity<?> updateDriver(@Valid @RequestBody Driver driver) {
        driverService.updateDriver(driver);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(value = "/delete")
    public ResponseEntity<?> deleteDriver(@RequestParam(value = "email") String email) {
        driverService.deleteDriverByEmail(email.trim());
        rabbitMQ.emailNotificationsDelete(email.trim());
        return ResponseEntity.ok().build();
    }

}
