package com.test.driver.util.mapping.driver;

import com.test.driver.model.restDTO.DriverResponseDTO;
import com.test.driver.model.restDTO.OrderDTO;
import com.test.driver.util.mapping.Order.OrderToOrderDTO;
import db.entity.Driver;

import java.util.List;

public class DriverToDriverDTO {

    public static DriverResponseDTO convertingDriverToDriverDTO(Driver driver) {
        List<OrderDTO> orderDTOS = OrderToOrderDTO.convertingOrderToOrderDTO(driver.getOrderFinished());
        DriverResponseDTO buildDriverDTO = DriverResponseDTO.builder()
                .nameDepartment(driver.getDepartment().getName())
                .orderDTOList(orderDTOS)
                .email(driver.getEmail())
                .name(driver.getName())
                .build();

        buildDriverDTO.setPassword(null);

        return buildDriverDTO;
    }

}
