package com.test.driver.service;

import com.test.driver.exception.DepartmentException;
import com.test.driver.exception.DriverException;
import com.test.driver.repository.DepartmentRepository;
import com.test.driver.repository.DriverRepository;
import db.entity.Department;
import db.entity.Driver;
import enam.Role;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DriverService {

    private final DriverRepository driverRepository;

    private final DepartmentRepository departmentRepository;

    @Transactional
    public Driver getDriverByEmail(String emailDriver) {
        return driverRepository.findByEmail(emailDriver)
                .orElseThrow(() -> new DriverException("There is no driver with this email: " + emailDriver));
    }

    @Transactional
    public Driver saveDriver(Driver driver, String department) {
        if (driver.getEmail().endsWith("@yandex.ru")) {
            throw new DriverException("Email should not end with characters @yandex.ru");
        }

        //Проверяем почту до как ложем Driver в Department. Чтоб он не попал в cache Hibernate.
        boolean isPresent = driverRepository.findByEmail(driver.getEmail()).isPresent();
        if (isPresent) {
            throw new DriverException("Email is already busy: " + driver.getEmail());
        }

        Department departmentActual = departmentRepository.findByName(department)
                .orElseThrow(() -> new DepartmentException("There is no department with that name: " + department));

        departmentActual.setDriverList(Optional.of(driver).stream().collect(Collectors.toList()));
        driver.setDepartment(departmentActual);
        driver.setPassword(new BCryptPasswordEncoder().encode(driver.getPassword()));
        driver.setRole(Role.DRIVER);

        return driverRepository.save(driver);
    }

    @Transactional
    public Driver getDriverFullByEmail(String emailDriver) {
        return driverRepository.findByEmailFullDriver(emailDriver)
                .orElseThrow(() -> new DriverException("There is no driver with this email: " + emailDriver));
    }

    @Transactional
    public Driver updateDriver(Driver driver) {
        Driver driverActual = driverRepository.findByEmail(driver.getEmail())
                .orElseThrow(() -> new DriverException("There is no driver with this email: " + driver.getEmail()));

        if (driver.getPassword() != null && !driver.getPassword().equals(driverActual.getPassword())) {
            driverActual.setPassword(new BCryptPasswordEncoder().encode(driver.getPassword()));
        }

        driverActual.setName(driver.getName());

        return driverRepository.save(driverActual);
    }

    @Transactional
    public boolean deleteDriverByEmail(String email) {
        driverRepository.findByEmail(email)
                .orElseThrow(() -> new DriverException("There is no driver with this email: " + email));
        driverRepository.deleteByEmail(email);
        return true;
    }

}
