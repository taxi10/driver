package com.test.driver.repository;


import db.entity.Driver;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface DriverRepository extends JpaRepository<Driver, Long> {

    void deleteByEmail(String emailDriver);

    Optional<Driver> findByEmail(String emailDriver);

    @Query(value = "select d from Driver d " +
            "left join d.orderFinished ord on d.id = ord.driver.id " +
            "left join d.department dep on dep.id = d.department.id " +
            "where d.email = :email")
    Optional<Driver> findByEmailFullDriver(@Param(value = "email") String emailDriver);

}
