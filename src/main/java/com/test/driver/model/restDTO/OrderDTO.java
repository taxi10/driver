package com.test.driver.model.restDTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class OrderDTO {

    private String description;

    private String timeCreateOrder;

    private String timeCompletionOrder;

    private String status;

}
