package com.test.driver.model.restDTO;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class DriverResponseDTO {

    private String name;

    private String email;

    private String password;

    private String role;

    private String nameDepartment;

    List<OrderDTO> orderDTOList;

}
