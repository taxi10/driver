package com.test.driver.Config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.driver.model.restDTO.DriverResponseDTO;
import db.entity.Driver;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class RabbitMQ {

    private static final String TOPIC_EXCHANGE_DRIVER = "driver.exchange";

    private static final String ROUTING_KEY_DRIVER_REGISTRATION = "key.driver.registration";
    private static final String ROUTING_KEY_DRIVER_DELETE = "key.driver.delete";

    private final RabbitTemplate rabbitTemplate;

    private final ObjectMapper objectMapper;

    @SneakyThrows
    public void emailNotificationsRegistration(Driver driver) {
        rabbitTemplate
                .convertAndSend(TOPIC_EXCHANGE_DRIVER,
                        ROUTING_KEY_DRIVER_REGISTRATION,
                        objectMapper.writeValueAsString(DriverResponseDTO
                                .builder()
                                .email(driver.getEmail())
                                .name(driver.getName())
                                .build()));
    }

    @SneakyThrows
    public void emailNotificationsDelete(String emailDriver) {
        rabbitTemplate
                .convertAndSend(TOPIC_EXCHANGE_DRIVER,
                        ROUTING_KEY_DRIVER_DELETE,
                        emailDriver);
    }

}
