__Данный сервис принимает все запросы свзяанные с driver.__

- `registration`

http://130.193.53.230:8989/driver/registration `POST`  `Не требуется авторизация`

- Params
    - (key) `department` (value) `"name department"` (Обязательный)

- body(JSON)
    - (key) `name:` (value) `custom name` (Обязательный)
    - (key) `email:` (value) `custom email` (Обязательный)
    - (key) `password":` (value) `custom password"` (Обязательный)

`> http://130.193.53.230:8989/driver/registration?department=Test`

```
{
    "name": "Test",
    "email": "Test@mail.ru",
    "password": "12345678"
}
```

> Данный url регистрирует driver. 
---
---
---
  
- `full-driver`

http://130.193.53.230:8989/driver/full `GET` `Требуется авторизация`[<sup>авторизация в postman</sup>](#f1)

- Params
    - (key) `email` (value) `"email driver"` (Обязательный)

`> http://130.193.53.230:8989/driver/full?email=Test@mail.ru`

> Данный url возвращает driver со всеми его вложинимы завитимостими. `order` `department`
---
---
---
- `update`

http://130.193.53.230:8989/driver/update `PUT` `Требуется авторизация`[<sup>авторизация в postman</sup>](#f1)

- body(JSON)
  - (key) `name:` (value) `custom name` (Обязательный)
  - (key) `email:` (value) `custom email` (Обязательный)
  - (key) `password":` (value) `custom password"` (Необязательный)

`> http://130.193.53.230:8989/driver/update`

```
{
    "name": "Test", 
    "email": "Test@mail.ru",
    "password": "12345678"
}
```

> Данный url обновляет driver. Для обновления доступны только `name` и `password`.
---
---
---

- `delete`

http://130.193.53.230:8989/driver/delete `DELETE` `Требуется авторизация`[<sup>авторизация в postman</sup>](#f1)

- Params
  - (key) `email` (value) `"email driver"` (Обязательный)
  

`> http://130.193.53.230:8989/driver/delete?email=Test@mail.ru`

> Данный url удаляет driver по email`.
---
---
---
- <span id="f1">авторизация в postman</span> :
> Переходим в вкладку `authorization` 

<img src="images/authorization.png" width="800" height="400">

> В Type выбираем `basic auth` 

<img src="images/basicAuth1.png" width="800" height="400">

> В поле `Username` и `Password` вводим ранее зарегистрированные данные 

<img src="images/basicAuth.png" width="800" height="400">

